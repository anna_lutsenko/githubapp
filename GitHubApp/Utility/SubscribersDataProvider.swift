//
//  SubscribersDataProvider.swift
//  GitHubApp
//
//  Created by Anna on 29.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import UIKit
class SubscribersDataProvider {
    private var requestManager = RequestManager()
    private (set) var repository: Repository
    private (set) var subscribers = [Subscriber]()
    
    private var isLoading = false
    private var isLastArray = false
    private var currentPage = 1
    private let perPage = 30
    
    var shouldLoadNextPage : Bool {
        return !isLastArray && !isLoading
    }
    
    init(with repository: Repository) {
        self.repository = repository
    }
    
    func getSubscribers(succeess: @escaping ([Subscriber])->(), failure: @escaping (Error)->()) {
        currentPage = 1
        isLoading = true
        requestManager.getSubscribers(url: repository.subscribersURL!, page: 1, succeess: { subscribers in
            self.isLoading = false
            self.subscribers = subscribers
            succeess(subscribers)
        }) { error in
            self.isLoading = false
            failure(error)
        }
    }
    
    func nextPage(succeess: @escaping ([Subscriber])->(), failure: @escaping (Error)->()) {
        currentPage += 1
        isLoading = true
        requestManager.getSubscribers(url: repository.subscribersURL!, page: currentPage, succeess: { subscribers in
            self.isLoading = false
            self.isLastArray = subscribers.count < self.perPage
            self.subscribers += subscribers
            succeess(subscribers)
        }) { error in
            self.isLoading = false
            failure(error)
        }
    }
    
    func getImageFor(indexCell:Int, succeess: @escaping (UIImage)->(), failure: @escaping (Error)->()) {
        let url = self.subscribers[indexCell].avatarURL
        
        requestManager.getImage(url: url, success: { img in
            guard let image = img else {
                failure(RequestError.unknownError)
                return
            }
            succeess(image)
        }) { error in
            failure(error)
        }
    }
}
