//
//  DataProvider.swift
//  GitHubApp
//
//  Created by Anna on 28.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import UIKit

class RepositoryDataProvider {
    
    private let requestManager = RequestManager()
    private var searchKeyWord = ""
    private var isLoading = false
    private var currentPage = 1
    private var totalCount = 0
    private (set) var repositories = [Repository]()
    
    var shouldLoadNext : Bool {
        return repositories.count < totalCount && !isLoading
    }
    
    func search(str: String, succeess: @escaping ([Repository])->(), failure: @escaping (Error)->())  {
        currentPage = 1
        searchKeyWord = str
        isLoading = true
        requestManager.searchRequest(str: str, page: currentPage, succeess: { result in
            self.isLoading = false
            self.totalCount = result.totalCount
            self.repositories = result.repositories
            succeess(result.repositories)
        }) { (error) in
            self.isLoading = false
            failure(error)
        }
    }
    
    func nextPage(succeess: @escaping ([Repository])->(), failure: @escaping (Error)->()) {
        currentPage += 1
        isLoading = true
        requestManager.searchRequest(str: searchKeyWord, page: currentPage, succeess: { result in
            self.isLoading = false
            self.totalCount = result.totalCount
            self.repositories += result.repositories
            succeess(result.repositories)
        }) { (error) in
            self.isLoading = false
            failure(error)
        }
    }
    
    func getImageFor(indexCell:Int, succeess: @escaping (UIImage)->(), failure: @escaping (Error)->()) {
        let url = self.repositories[indexCell].owner.avatarURL
      
        requestManager.getImage(url: url, success: { img in
            guard let image = img else {
                failure(RequestError.unknownError)
                return
            }
            succeess(image)
        }) { error in
            failure(error)
        }
    }
  
}
