//
//  RequestManager.swift
//  GitHubApp
//
//  Created by Anna on 28.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import Unbox

struct URLConstants {
    static let searchURL = "https://api.github.com/search/repositories"
}
enum RequestError : Error {
    case unknownError
}

class RequestManager {
    typealias Success = (SearchRepositories) -> Void
    typealias Failure = (Error) -> Void
    
    func searchRequest(str: String, page: Int, succeess: @escaping Success, failure: @escaping Failure) {
        
        let urlParams = ["q": str, "page": page] as [String : Any]
        
        Alamofire.request(URLConstants.searchURL, method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success(let json):
                    guard let unboxableDictionary = json as? UnboxableDictionary else {
                        failure(RequestError.unknownError)
                        return
                    }
                    do {
                        let searchResult: SearchRepositories = try unbox(dictionary: unboxableDictionary)
                        succeess(searchResult)
                    } catch let error {
                        failure(error)
                    }
                    break
                case .failure(let error):
                    failure(error)
                }
        }
    }
    
    func getImage(url: String?, success: @escaping (UIImage?)->(), failure: @escaping Failure) {
        guard let url = url else {
            success(nil)
            return
        }
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                success(image)
            } else {
                success(nil)
            }
        }
    }
    
    func getSubscribers(url: String, page: Int, succeess: @escaping ([Subscriber])->(), failure: @escaping Failure) {
        
        let urlParams = ["page": page] as [String : Any]
        
        Alamofire.request(url, method: .get, parameters: urlParams)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                switch response.result {
                case .success(let json):
                    guard let unboxableDictionary = json as? [UnboxableDictionary] else {
                        failure(RequestError.unknownError)
                        return
                    }
                    do {
                        let subscribers: [Subscriber] = try unbox(dictionaries: unboxableDictionary)
                        succeess(subscribers)
                    } catch let error {
                        failure(error)
                    }
                    break
                case .failure(let error):
                    failure(error)
                }
        }
    }
    
}
