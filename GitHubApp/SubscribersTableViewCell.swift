//
//  SubscribersTableViewCell.swift
//  GitHubApp
//
//  Created by Anna on 29.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class SubscribersTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
