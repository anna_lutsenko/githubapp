//
//  RepositoryTableViewController.swift
//  GitHubApp
//
//  Created by Anna on 27.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit
import SVProgressHUD

class RepositoryTableViewController: UITableViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    
    let dataProvider = RepositoryDataProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchBar.delegate = self
        searchBar.becomeFirstResponder()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.repositories.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let repository = dataProvider.repositories[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! RepositoryTableViewCell
       
        cell.avatar.image = nil
        cell.labelRepositoryName.text = repository.name
        cell.labelDescription.text = repository.description
        cell.labelForksCount.text = "\(String(describing: repository.forksCount)) forks"
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let repository = dataProvider.repositories[indexPath.row]
        
        let vc = SubscribersTableViewController.instanceVC()
        vc.dataProvider = SubscribersDataProvider(with: repository)
        self.navigationController?.pushViewController(vc, animated: true)

    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let isLastCell = (indexPath.row == (dataProvider.repositories.count - 1))
        
        dataProvider.getImageFor(indexCell: indexPath.row, succeess: { img in
            DispatchQueue.main.async {
                let cell = tableView.cellForRow(at: indexPath) as? RepositoryTableViewCell
                cell?.avatar.image = img
            }
        }) { error in
            
        }
        
        if (isLastCell && dataProvider.shouldLoadNext) {
            SVProgressHUD.show()
            dataProvider.nextPage(succeess: { _ in
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }, failure: { error in
                SVProgressHUD.dismiss()
            })
        }
    }
    
}

extension RepositoryTableViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if searchBar.text != "" {
            SVProgressHUD.show()
            dataProvider.search(str: searchBar.text!, succeess: { _ in
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }, failure: { error in
                SVProgressHUD.dismiss()
            })
        }
    }
}
