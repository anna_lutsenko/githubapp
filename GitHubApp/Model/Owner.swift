//
//  Owner.swift
//  GitHubApp
//
//  Created by Anna on 28.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import Unbox

struct Owner {
    var avatarURL: String?
}

extension Owner : Unboxable {
    init(unboxer: Unboxer) throws {
        self.avatarURL = try? unboxer.unbox(key: "avatar_url")
    }
}
