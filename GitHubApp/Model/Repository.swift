//
//  Repository.swift
//  GitHubApp
//
//  Created by Anna on 28.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import Unbox

struct Repository {
    var name: String
    var description: String?
    var forksCount: Int
    var owner: Owner
    var subscribersURL: String?
}

extension Repository: Unboxable {

    init(unboxer: Unboxer) throws {
        self.name = try unboxer.unbox(key: "name")
        self.description = try? unboxer.unbox(key: "description")
        self.forksCount = try unboxer.unbox(key: "forks_count")
        self.owner = try unboxer.unbox(key: "owner")
        self.subscribersURL = try? unboxer.unbox(key: "subscribers_url")
    }
}

