//
//  Subscriber.swift
//  GitHubApp
//
//  Created by Anna on 29.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import Unbox

struct Subscriber {
    var login: String
    var avatarURL: String?
}

extension Subscriber : Unboxable {
    init(unboxer: Unboxer) throws {
        self.login = try unboxer.unbox(key: "login")
        self.avatarURL = try? unboxer.unbox(key: "avatar_url")
    }
}
