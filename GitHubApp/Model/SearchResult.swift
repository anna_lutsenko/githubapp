//
//  SearchResult.swift
//  GitHubApp
//
//  Created by Anna on 28.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import Foundation
import Unbox

struct SearchRepositories {
    var totalCount: Int
    var repositories: [Repository]
}

extension SearchRepositories: Unboxable {
    init(unboxer: Unboxer) throws {
        self.totalCount = try unboxer.unbox(key: "total_count")
        self.repositories = try unboxer.unbox(key: "items")
    }
}
