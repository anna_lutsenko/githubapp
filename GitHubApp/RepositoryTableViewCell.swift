//
//  RepositoryTableViewCell.swift
//  GitHubApp
//
//  Created by Anna on 27.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelRepositoryName: UILabel!
    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelForksCount: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
