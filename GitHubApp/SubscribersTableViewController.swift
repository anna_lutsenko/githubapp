//
//  SubscribersTableViewController.swift
//  GitHubApp
//
//  Created by Anna on 29.10.17.
//  Copyright © 2017 Anna Lutsenko. All rights reserved.
//

import UIKit
import SVProgressHUD

class SubscribersTableViewController: UITableViewController {
    
    @IBOutlet weak var labelSubscribers: UILabel!
    
    var dataProvider : SubscribersDataProvider! = nil
    var totalCount = 0 {
        didSet {
            self.labelSubscribers.text = "\(totalCount) subscribers"
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = dataProvider.repository.name
        self.downloadSubscribers()
    }
    
    static func instanceVC() -> SubscribersTableViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SubscribersTableViewController") as! SubscribersTableViewController
        return vc
    }
    
    func downloadSubscribers() {
        SVProgressHUD.show()
        dataProvider.getSubscribers(succeess: {_ in
            SVProgressHUD.dismiss()
            DispatchQueue.main.async {
                self.totalCount = self.dataProvider.subscribers.count
                self.tableView.reloadData()
            }
        }) { error in
            SVProgressHUD.dismiss()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.subscribers.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let subscriber = dataProvider?.subscribers[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseCell", for: indexPath) as! SubscribersTableViewCell
        
        cell.avatar.image = nil
        cell.labelName.text = subscriber?.login
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        dataProvider?.getImageFor(indexCell: indexPath.row, succeess: { img in
            DispatchQueue.main.async {
                let cell = tableView.cellForRow(at: indexPath) as? SubscribersTableViewCell
                cell?.avatar.image = img
            }
        }) { error in
            
        }
        
        let isLastCell = (dataProvider.subscribers.count - 1) == indexPath.row
        
        if isLastCell && dataProvider.shouldLoadNextPage {
            SVProgressHUD.show()
            dataProvider.nextPage(succeess: { _ in
                SVProgressHUD.dismiss()
                DispatchQueue.main.async {
                    self.totalCount = self.dataProvider.subscribers.count
                    self.tableView.reloadData()
                }
            }, failure: { error in
                SVProgressHUD.dismiss()
            })
        }
    }
  
}
